{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Automata.RegEx where

import Automata.Base
import Automata.Rules
import Automata.NFA
import Control.Monad.Identity
import Control.Monad.Supply

data Pattern = Empty | 
               Literal Char | 
               Concatenate Pattern Pattern |
               Choose Pattern Pattern |
               Repeat Pattern

type Precedence = Int

bracket :: Precedence -> Pattern -> String
bracket outer pa = if precedence pa < outer then "(" ++ show pa ++ ")" else show pa

instance Show Pattern where
    show p = case p of
        Empty -> "" 
        Literal ch -> [ch]
        cc@(Concatenate a b) -> br a ++ "." ++ br b where br = bracket (precedence cc)
        ch@(Choose a b) -> br a ++ "|" ++ br b where br = bracket (precedence ch)
        r@(Repeat p) -> bracket (precedence r) p ++ "*"

precedence :: Pattern -> Precedence
precedence p = case p of Empty -> 3; Literal _ -> 3; Concatenate _ _ -> 1; Choose _ _ -> 0; Repeat _ -> 2

toNFA :: Pattern -> NFA
toNFA = runSupplyVars . toNFAUsingSupply 
    where runSupplyVars x = fst (runSupply x [(1::Int)..]) where vars = fmap (\x -> [x]) [(1::Int)..]

toNFAUsingSupply :: MonadSupply State m => Pattern -> m NFA
toNFAUsingSupply p = case p of
    Empty -> emptyNFA
    Literal ch -> literalNFA ch
    Concatenate a b -> concatenateNFA a b
    Choose a b -> chooseNFA a b
    Repeat a -> repeatNFA a

emptyNFA :: MonadSupply State m => m NFA
emptyNFA = do
    s <- supply
    return $ mkNFA s [s] $ NFARulebook []

literalNFA :: MonadSupply State m => Symbol -> m NFA
literalNFA ch = do
    s <- supply  
    a <- supply
    return $ mkNFA s [a] $ NFARulebook [FARule s (T ch) a]

concatenateNFA :: MonadSupply State m => Pattern -> Pattern -> m NFA
concatenateNFA a b = do
    na <- toNFAUsingSupply a
    nb <- toNFAUsingSupply b
    return $ concatenatedNFA na nb

concatenatedNFA :: NFA -> NFA -> NFA
concatenatedNFA na nb =
    let (NFARulebook rulesA) = nfaRulebook na
        (NFARulebook rulesB) = nfaRulebook nb
        extraRules = fmap (\p -> FARule p Epsilon (startState nb)) (nfaAcceptStates na)
        rulebook = NFARulebook (rulesA ++ rulesB ++ extraRules) 
    in mkNFA (startState na) (nfaAcceptStates nb) rulebook

chooseNFA :: MonadSupply State m => Pattern -> Pattern -> m NFA
chooseNFA a b = do
    s <- supply
    na <- toNFAUsingSupply a
    nb <- toNFAUsingSupply b
    return $ chosenNFA s na nb

chosenNFA :: State -> NFA -> NFA -> NFA
chosenNFA start na nb =
    let (NFARulebook rulesA) = nfaRulebook na
        (NFARulebook rulesB) = nfaRulebook nb
        acceptStates = (nfaAcceptStates na) ++ (nfaAcceptStates nb)
        extraRules = fmap (\nfa -> FARule start Epsilon (startState nfa)) [na, nb]
        rulebook = NFARulebook (rulesA ++ rulesB ++ extraRules)
    in mkNFA start acceptStates rulebook

repeatNFA :: MonadSupply State m => Pattern -> m NFA
repeatNFA a = do
    s <- supply
    na <- toNFAUsingSupply a
    return $ repeatedNFA s na

repeatedNFA :: State -> NFA -> NFA
repeatedNFA start na =
    let (NFARulebook rulesA) = nfaRulebook na
        oldStartState = startState na
        acceptStates = (nfaAcceptStates na)
        startIsAcceptedRule = FARule start Epsilon oldStartState
        extraRules2 = fmap (\as -> FARule as Epsilon oldStartState) acceptStates 
        rulebook = NFARulebook (rulesA ++ (startIsAcceptedRule : extraRules2))
    in mkNFA start (start:acceptStates) rulebook    

matches :: String -> Pattern -> Bool
matches s p = acceptingString s (toNFA p)

examplePattern = Repeat $ Concatenate (Literal 'a') (Choose (Literal 'b') (Literal 'c'))