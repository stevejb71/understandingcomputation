module Automata.Parser where

import Control.Applicative ((<*))
import Text.ParserCombinators.Parsec.Combinator
import Text.ParserCombinators.Parsec
import Automata.Base
import Automata.RegEx

parseRegEx :: String -> Either ParseError Pattern
parseRegEx input = doParse regex input

doParse p = parse p "unknown"

elements = bracketed <|> literal
regex = try concatenated <|> try chosen <|>  try repeated <|> elements

literal :: GenParser Char st Pattern
literal = do
    ch <- noneOf "()*|"
    return $ Literal ch

concatenated :: GenParser Char st Pattern
concatenated = do
    a <- elements
    b <- regex
    return $ Concatenate a b

bracketed :: GenParser Char st Pattern
bracketed = do
    char '('
    s <- regex
    char ')'
    return s

repeated ::  GenParser Char st Pattern
repeated = do
    s <- elements
    char '*'
    return $ Repeat s

chosen ::  GenParser Char st Pattern
chosen = do
    a <- elements
    char '|'
    b <- regex
    return $ Choose a b

itMatches :: String -> String -> Bool
itMatches r s = let (Right p) = parseRegEx r in matches s p