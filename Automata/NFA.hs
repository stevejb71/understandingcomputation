module Automata.NFA where

import Automata.Base
import Automata.Rules
import Data.List (intersect, foldl', nub)

data NFARulebook = NFARulebook [FARule] deriving Show

-- The guts of the machine.

nextStates :: [State] -> Transition -> NFARulebook -> [State]
nextStates sts ch rb = concatMap (\st -> (fmap follow) (rulesFor st rb)) sts
    where rulesFor st (NFARulebook rs) = filter (appliesTo st ch) rs 

followAll :: [State] -> Char -> NFARulebook -> [State]
followAll ss ch nfa = (nextStates (followEpsilonMoves ss nfa) (T ch) nfa)

followEpsilonMoves :: [State]-> NFARulebook -> [State]
followEpsilonMoves ss nfa = 
    if epsilonMoveStates `isSubsetOf` ss then ss else followEpsilonMoves (nub $ ss ++ epsilonMoveStates) nfa
    where epsilonMoveStates = nextStates ss Epsilon nfa

-- Construction

add :: NFARulebook -> (State, [Char], State) -> NFARulebook
add (NFARulebook rs) (s1, cs, s2) = NFARulebook (rs ++ (fmap (\c -> FARule s1 (T c) s2) cs)) 

addEpsilon :: NFARulebook -> (State, [State]) -> NFARulebook
addEpsilon (NFARulebook rs) (s1, s2) = NFARulebook (rs ++ fmap (\s -> FARule s1 Epsilon s) s2) 

emptyRulebook :: NFARulebook
emptyRulebook = NFARulebook []

-- Accepting rules

readString :: String -> NFA -> NFA
readString = flip $ foldl' (flip readCharacter) 
    where readCharacter ch nfa@(NFA cs _ rb) = setStates (followAll cs ch rb) nfa
          setStates s (NFA _ as rb) = NFA s as rb

acceptingString :: String -> NFA -> Bool
acceptingString s = accepting . readString s
    where accepting (NFA cs as _) = not $ null (cs `intersect` as)

-- NFA and examples

data NFA = NFA {nfaCurrentStates :: [State], nfaAcceptStates :: [State], nfaRulebook :: NFARulebook} deriving Show

mkNFA :: State -> [State] -> NFARulebook -> NFA
mkNFA current accept rb = NFA [current] accept rb

startState :: NFA -> State
startState nfa = head $ nfaCurrentStates nfa

exampleNFA :: NFA
exampleNFA = mkNFA 1 [4] $ emptyRulebook `add` (1, "ab", 1) `add` (1, "b", 2) `add` (2, "ab", 3) `add` (3, "ab", 4)

mult3Or2AttemptNFA :: NFA
mult3Or2AttemptNFA = mkNFA 1 [1] $ emptyRulebook `add` (1, "a", 2) `add` (2, "a", 1) `add` (1, "a", 3) `add` (3, "a", 4) `add` (4, "a", 1)

mult3Or2WithEpsilonNFA :: NFA
mult3Or2WithEpsilonNFA = mkNFA 1 [2, 4] $ emptyRulebook `addEpsilon` (1, [2, 4]) `add` (2, "a", 3) `add` (3, "a", 2) `add` (4, "a", 5) `add` (5, "a", 6) `add` (6, "a", 4)