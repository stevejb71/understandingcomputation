module Automata.Base where

import Data.List (intersect)

type State = Int
type Symbol = Char
data Transition = T Symbol | Epsilon deriving Eq

instance Show Transition where
    show (T c) = show c
    show Epsilon = "."

isSubsetOf l1 l2 = (l1 `intersect` l2) == l1