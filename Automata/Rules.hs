module Automata.Rules where

import Automata.Base

data FARule = FARule State Transition State

instance Show FARule
    where show (FARule s t a) = show s ++ "-" ++ show t ++ "->" ++ show a

toInputs :: String -> [Transition]
toInputs s = fmap T s

appliesTo :: State -> Transition -> FARule -> Bool
appliesTo st ch (FARule curr rch _) = curr == st && rch == ch

follow :: FARule -> State
follow (FARule _ _ next) = next