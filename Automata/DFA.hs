module Automata.DFA where

import Data.List (find, foldl')
import Data.Maybe (fromJust)
import Automata.Base
import Automata.Rules

data DFARulebook = DFARulebook [FARule] deriving Show

nextState ::  State -> Transition -> DFARulebook -> State
nextState st ch rb = follow (ruleFor st ch rb)

ruleFor :: State -> Transition -> DFARulebook -> FARule
ruleFor st ch (DFARulebook rs) = fromJust $ find (appliesTo st ch) rs

add :: DFARulebook -> (State, [Char], State) -> DFARulebook
add (DFARulebook rs) (s1, cs, s2) = DFARulebook (rs ++ (fmap (\c -> FARule s1 (T c) s2) cs)) 

emptyRulebook :: DFARulebook
emptyRulebook = DFARulebook []

data DFA = DFA {currentState :: State, acceptStates :: [State], dfaRulebook :: DFARulebook} deriving Show

setState :: State -> DFA -> DFA
setState s (DFA _ as rb) = DFA s as rb

accepting :: DFA -> Bool
accepting dfa = (currentState dfa) `elem` (acceptStates dfa)

readTransition :: Transition -> DFA -> DFA
readTransition ch dfa@(DFA cst _ rb) = setState (nextState cst ch rb) dfa

readTransitions :: [Transition] -> DFA -> DFA
readTransitions = flip $ foldl' (flip readTransition)

acceptingTransitions :: [Transition] -> DFA -> Bool
acceptingTransitions s = accepting . readTransitions s

-- Example values
exampleDfa :: DFA
exampleDfa = DFA 1 [3] $ emptyRulebook `add` (1, "a", 2) `add` (1, "b", 1) `add` (2, "a", 2) `add` (2, "b", 3) `add` (3, "ab", 3) 

thirdCharBDFA :: DFA
thirdCharBDFA = DFA 1 [5] $ emptyRulebook `add` (1, "ab", 2) `add` (2, "ab", 3) `add` (3, "a", 4) `add` (3, "b", 5) `add` (4, "ab", 4) `add` (5, "ab", 5)
