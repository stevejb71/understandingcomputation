{-# LANGUAGE TemplateHaskell #-}

module Automata.PDARules where

import Automata.Base
import Control.Lens

type Stack = []

push :: a -> Stack a -> Stack a
push = (:)

pop :: Stack a -> Maybe (a, Stack a)
pop [] = Nothing
pop (x:xs) = Just (x, xs)

data PDAConfiguration = PDAConfiguration {_pdaConfigState :: State, _pdaStack :: Stack Char} deriving Show
makeLenses ''PDAConfiguration

data PDARule = PDARule {_pdaState :: State, _pdaCh :: Transition, _pdaNextState :: State, _popCh :: Symbol, _pushChs :: [Symbol]} deriving Show
makeLenses ''PDARule

newtype DPDARulebook = DPDARulebook [PDARule] deriving Show
