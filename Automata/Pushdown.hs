{-# LANGUAGE TemplateHaskell #-}

module Automata.Pushdown where

import Automata.Base
import Automata.PDARules
import Data.List (find, foldl')
import Data.Maybe (fromJust)
import Control.Lens

data DPDA = DPDA {_dpdaConfig :: PDAConfiguration, _dpdaAcceptStates :: [State], _dpdaRulebook :: DPDARulebook} deriving Show
makeLenses ''DPDA

pdaRule :: State -> Symbol -> State -> Symbol -> [Symbol] -> PDARule
pdaRule st sym nst po pu = PDARule st (T sym) nst po pu

appliesTo :: PDAConfiguration -> Transition -> PDARule -> Bool
appliesTo (PDAConfiguration st chs) ch r = 
    let popped = fmap fst $ pop chs
    in r^.pdaState == st && Just (r^.popCh) == popped && (r^.pdaCh) == ch

followAll :: PDAConfiguration -> DPDARulebook -> PDARule -> PDAConfiguration
followAll c rb r = followEpsilonMoves (PDAConfiguration (r^.pdaNextState) (nextStack c r)) rb

follow :: PDAConfiguration -> PDARule -> PDAConfiguration
follow c rb = PDAConfiguration (rb^.pdaNextState) (nextStack c rb)

followEpsilonMoves :: PDAConfiguration -> DPDARulebook -> PDAConfiguration
followEpsilonMoves c rb = case ruleFor c Epsilon rb of
    Nothing -> c
    Just _ -> followEpsilonMoves (fromJust $ nextEpsilonConfiguration rb c) rb

followEpsilonMoves2 :: DPDA -> DPDA
followEpsilonMoves2 d = DPDA (followEpsilonMoves (d^.dpdaConfig) (d^.dpdaRulebook)) (d^.dpdaAcceptStates) (d^.dpdaRulebook)

nextStack :: PDAConfiguration -> PDARule -> Stack Char
nextStack (PDAConfiguration _ stack) r = 
    let (_, poppedStack) = fromJust $ pop stack 
    in foldl' (\s c -> c `push` s) poppedStack (reverse (r^.pushChs))

nextConfigurations ::  Transition -> DPDARulebook -> [PDAConfiguration] -> [Maybe PDAConfiguration]
nextConfigurations ch rb = fmap (nextConfiguration ch rb)

nextConfiguration ::  Transition -> DPDARulebook -> PDAConfiguration -> Maybe PDAConfiguration
nextConfiguration ch rb config = fmap (followAll config rb) (ruleFor config ch rb)

nextEpsilonConfiguration ::  DPDARulebook -> PDAConfiguration -> Maybe PDAConfiguration
nextEpsilonConfiguration rb config = fmap (follow config) (ruleFor config Epsilon rb)

ruleFor :: PDAConfiguration -> Transition -> DPDARulebook -> Maybe PDARule
ruleFor config ch (DPDARulebook rs) = find (appliesTo config ch) rs

accepting :: DPDA -> Bool
accepting p = p^.dpdaConfig.pdaConfigState `elem` p^.dpdaAcceptStates

readString :: String -> DPDA -> Maybe DPDA
readString s dp = foldl' readCharacter (Just dp) s 
    where readCharacter (Just d) ch = fmap (\c -> DPDA c (d^.dpdaAcceptStates) rb) (nextConfiguration (T ch) rb (d^.dpdaConfig)) where rb = d^.dpdaRulebook
          readCharacter Nothing _ = Nothing

acceptingString :: String -> DPDA -> Bool
acceptingString s d = maybe False accepting (readString s d) 

pdaConfig :: PDAConfiguration
pdaConfig = PDAConfiguration 1 ['$']

rulebook :: DPDARulebook
rulebook = DPDARulebook $ [
    mkPDARule 1 '(' 2 '$' "b$", 
    mkPDARule 2 '(' 2 'b' "bb",
    mkPDARule 2 ')' 2 'b' "",
    PDARule 2 Epsilon 1 '$' "$"
    ]

eqAsAndBs :: DPDARulebook
eqAsAndBs = DPDARulebook $ [
    mkPDARule 1 'a' 2 '$' "a$",
    mkPDARule 1 'b' 2 '$' "b$",
    mkPDARule 2 'a' 2 'a' "aa",
    mkPDARule 2 'b' 2 'b' "bb",
    mkPDARule 2 'a' 2 'b' "",
    mkPDARule 2 'b' 1 'a' "",
    PDARule 2 Epsilon 1 '$' "$"
    ]

dpda :: DPDA
dpda = DPDA pdaConfig [1] rulebook

eqAB :: DPDA
eqAB = DPDA (PDAConfiguration 1 "$") [1] eqAsAndBs

mkPDARule :: State -> Symbol -> State -> Symbol -> [Symbol] -> PDARule
mkPDARule st ch nst pp pu = PDARule st (T ch) nst pp pu
