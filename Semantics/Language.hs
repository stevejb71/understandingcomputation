{-# LANGUAGE GADTs #-}

module Semantics.Language where

import qualified Data.Map.Strict as M
import Data.Map ((!)) 

data LeafExpr a where
    Boolean ::  Bool -> LeafExpr Bool 
    Number :: Int -> LeafExpr Int

data NodeExpr a where
    Variable :: String -> NodeExpr Int
    LessThan :: Expr Int -> Expr Int -> NodeExpr Bool
    Add :: Expr Int -> Expr Int -> NodeExpr Int
    Multiply :: Expr Int -> Expr Int -> NodeExpr Int

data Expr a = Leaf (LeafExpr a) | Node (NodeExpr a)

number :: Int -> Expr Int
number n = Leaf $ Number n 

variable :: String -> Expr Int
variable v = Node $ Variable v 

add :: Expr Int -> Expr Int -> Expr Int
add l r = Node $ Add l r

multiply :: Expr Int -> Expr Int -> Expr Int
multiply l r = Node $ Multiply l r 

lessThan :: Expr Int -> Expr Int -> Expr Bool
lessThan l r = Node $ LessThan l r

type Env = M.Map String Int
mkEnv :: [(String, Int)] -> Env
mkEnv xs = M.fromList xs

unsafeIndex :: Env -> String -> Int 
unsafeIndex env k = if k `M.notMember` env then error (k ++ " is not in the map") else env ! k

instance Show a => Show (LeafExpr a) where
    show (Boolean b) = show b
    show (Number n) = show n

instance Show a => Show (NodeExpr a) where
    show (Variable v) = v
    show (LessThan l r) = show l ++ " < " ++ show r
    show (Add l r) = show l ++ " + " ++ show r
    show (Multiply l r) = show l ++ " * " ++ show r

instance Show a => Show (Expr a) where
    show (Leaf l) = show l
    show (Node n) = show n

data Statement = DoNothing | Assign String (Expr Int) | If (Expr Bool) Statement Statement | Sequence Statement Statement | While (Expr Bool) Statement

instance Show Statement where
    show (DoNothing) = "{}"
    show (Assign v e) = v ++ " := " ++ show e
    show (If cond ifTrue ifFalse) = "if " ++ show cond ++ " then " ++ show ifTrue ++ " else " ++ show ifFalse
    show (Sequence s1 s2) = show s1 ++ " ; " ++ show s2
    show (While cond body) = "while " ++ show cond ++ " do " ++ show body