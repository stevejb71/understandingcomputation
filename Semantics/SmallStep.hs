{-# LANGUAGE GADTs #-}

module Semantics.SmallStep where

import Data.Maybe (fromJust, isNothing)
import qualified Data.Map.Strict as M
import Semantics.Language
import Debug.Trace (trace)

reduceExpression :: Env -> NodeExpr a -> Expr a
reduceExpression env expr = case expr of
    Variable v -> Leaf $ Number (env `unsafeIndex` v)
    LessThan (Node l) r -> Node $ LessThan (reduceExpression env l) r
    LessThan l (Node r) -> Node $ LessThan l (reduceExpression env r)
    LessThan (Leaf (Number l)) (Leaf (Number r)) -> Leaf $ Boolean (l < r)
    Add (Node l) r -> Node $ Add (reduceExpression env l) r
    Add l (Node r) -> Node $ Add l (reduceExpression env r)
    Add (Leaf (Number l)) (Leaf (Number r)) -> Leaf $ Number (l+r)
    Multiply (Node l) r -> Node $ Multiply (reduceExpression env l) r
    Multiply l (Node r) -> Node $ Multiply l (reduceExpression env r)
    Multiply (Leaf (Number l)) (Leaf (Number r)) -> Leaf $ Number (l*r)

eval :: Show a => Env -> Expr a -> LeafExpr a
eval env expr = case expr of
    Leaf l -> l
    Node n -> trace (show n) (eval env (reduceExpression env n))

reduceStatement :: Env -> Statement -> Maybe (Env, Statement)
reduceStatement env stmt = case stmt of
    DoNothing -> Nothing
    Assign v (Leaf (Number n)) -> Just ((M.insert v n env), DoNothing) 
    Assign v e -> Just ((M.insert v n env), DoNothing) where (Number n) = eval env e 
    If (Leaf (Boolean b)) ifTrue ifFalse -> Just $ if b then (env, ifTrue) else (env, ifFalse)
    If cond ifTrue ifFalse -> Just $ (env, If (Leaf (eval env cond)) ifTrue ifFalse)
    Sequence DoNothing s2 -> Just (env, s2)
    Sequence s1 s2 -> reduceStatement env' (Sequence reduced s2)
        where Just (env', reduced) = reduceStatement env s1
    While cond body -> reduceStatement env $ If cond (Sequence body (While cond body)) DoNothing  

run :: Env -> Statement -> (Env, Statement)
run env stmt = if isNothing maybeReduced
    then (env, stmt) 
    else let (env', reduced) = fromJust maybeReduced
         in trace (show stmt ++ "     [" ++ (show env) ++ "]") (run env' reduced)
    where maybeReduced = reduceStatement env stmt
    
expressionExample :: LeafExpr Int
expressionExample = do
    let initial = mkEnv [("x", 3), ("y", 4)]
    eval initial (add (multiply (variable "x") (variable "y")) (add (number 5) (number 4)))

ifExample :: (Env, Statement)
ifExample = do
    let initial = mkEnv [("x", 3), ("y", 4)]
    run initial $ If (lessThan (variable "x") (number 1)) (Assign "x" (number 8)) (Assign "x" (number 9))

sequenceExample :: (Env, Statement)
sequenceExample = do
    let initial = mkEnv [("x", 3), ("y", 4)]
    let s1 = If (lessThan (variable "x") (number 1)) (Assign "x" (number 8)) (Assign "x" (number 9))
    let s2 = If (lessThan (variable "y") (number 5)) (Assign "y" (add (variable "x") (number 1))) (Assign "y" (number 9))
    run initial $ Sequence s1 s2

whileExample :: (Env, Statement)
whileExample = do
    let initial = mkEnv [("x", 5)]
    let s = While (lessThan (variable "x") (number 10)) (Assign "x" (add (number 1) (variable "x")))
    run initial $ s