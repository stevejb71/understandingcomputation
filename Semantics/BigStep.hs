{-# LANGUAGE GADTs #-}

module Semantics.BigStep where

import Semantics.Language
import qualified Data.Map.Strict as M
import Debug.Trace (traceShow)

eval :: Env -> Expr a -> a
eval env expr = case expr of
    Leaf (Number n) -> n
    Leaf (Boolean b) -> b
    Node (Variable v) -> eval env (number (env `unsafeIndex` v))
    Node (LessThan l r) -> eval env l < eval env r
    Node (Add l r) -> eval env l + eval env r
    Node (Multiply l r) -> eval env l * eval env r

run :: Env -> Statement -> Env
run env stmt = traceShow (stmt) (case stmt of
    DoNothing -> env
    Assign v e -> M.insert v (eval env e) env
    If cond ifTrue ifFalse -> run env $ if eval env cond then ifTrue else ifFalse
    Sequence s1 s2 -> run (run env s1) s2
    w@(While cond body) -> if eval env cond then run env $ Sequence body w else env) 

bigExample :: Env
bigExample = run initial $ While (lessThan (variable "x") (number 10)) (Assign "x" (add (variable "x") (number 1)))
    where initial = mkEnv [("x", 1)]