module Main where

import Semantics.BigStep
import Semantics.SmallStep
import Automata.NFA
import Automata.Pushdown

main :: IO ()
main = do
    print dpda